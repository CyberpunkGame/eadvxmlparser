#pragma once

#define __EXMLPARSER_NAME(_index, _name) __LIB2_FNE_NAME_LEFT(__E_FNENAME)##_##_name##_##_index##_

// 传递函数名和索引, 拼接成 定义库名_名字_序号_定义库名, 比如 exmlparser_test_0_exmlparser
#define EXMLPARSER_NAME(_index, _name) __LIB2_FNE_NAME_LEFT(__EXMLPARSER_NAME(_index, _name))__LIB2_FNE_NAME_LEFT(__E_FNENAME)

// 传递函数名和索引, 拼接成 "定义库名_名字_序号_定义库名", 比如 "exmlparser_test_0_exmlparser"
#define EXMLPARSER_NAME_STR(_index, _name) ______E_FNENAME(__EXMLPARSER_NAME(_index, _name))

// 这个宏定义了所有的命令, 以后需要命令名数组, 声明命令等, 都可以使用这个宏
// 下面为_MAKE宏参数列表, 除了第一个成员外, 其他都是 CMD_INFO 这个结构的成员
//  0 _index            = 命令索引, 会根据这个索引生成命令名, 这个索引也能让你更直观的看到这个命令是在命令数组里的下标
//  1 _szName           = 命令的中文名, 需要加双引号, 这个会显示在易语言支持库命令列表里
//  2 _szEgName         = 命令的英文名, 不能加双引号, 这个会显示在易语言支持库命令列表英文名字里, 会根据这个英文名字生成c++实现功能函数名, 这个也是静态库导出的符号名
//  3 _szExplain        = 命令详细解释, 需要加双引号, 这个会显示在易语言支持库命令列表的详细解释里
//  4 _shtCategory      = 全局命令的所属类别, 从1开始, 对象成员命令的此值为-1
//  5 _wState           = 标记, CT_开头常量, _CMD_OS(__OS_WIN) 表示支持win系统, _CMD_OS(OS_ALL) 表示支持所有系统, 如果返回数组或者变长参数, 需要或上对应标志位
//  6 _dtRetValType     = 返回值类型, 使用前注意转换HIWORD为0的内部数据类型值到程序中使用的数据类型值
//  7 _wReserved        = 保留字段, 填0
//  8 _shtUserLevel     = 命令的用户学习难度级别, LVL_SIMPLE=初级命令, LVL_SECONDARY=中级命令, LVL_HIGH=高级命令
//  9 _shtBitmapIndex   = 指定图像索引, 从1开始, 0表示无, 显示在支持库列表里的图标
// 10 _shtBitmapCount   = 图像数目(用作动画)
// 11 _nArgCount        = 命令的参数数目
// 12 _pBeginArgInfo    = 参数起始地址
#define EXMLPARSER_DEF(_MAKE) \
    _MAKE(  0, "加载文档", LoadXmlFile, "从指定文件加载XML文档。该方法执行成功返回真，执行失败返回假", \
    -1, _CMD_OS(OS_ALL), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 2, g_argumentInfo_exmlparser_global_var + 0)\
    _MAKE(  1, "解析文档", ParserXml, "解析字符串创建XML文档。该方法执行成功返回真，执行失败返回假", \
    -1, _CMD_OS(OS_ALL), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 1, g_argumentInfo_exmlparser_global_var + 2)\
    _MAKE(  2, "创建文档", CreateXml, "创建一个标准的XML文档。该方法执行成功返回真，执行失败返回假", \
    -1, _CMD_OS(OS_ALL), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 1, g_argumentInfo_exmlparser_global_var + 29)\
    _MAKE(  3, "生成文档内容", ToXmlString, "将XML文档转为文本内容，返回文本", \
    -1, _CMD_OS(OS_ALL), SDT_TEXT, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_exmlparser_global_var + 0)\
    _MAKE(  4, "保存文档", SaveXml, "将XML文档内容保存到指定文件。该方法执行成功返回真，执行失败返回假", \
    -1, _CMD_OS(OS_ALL), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 1, g_argumentInfo_exmlparser_global_var + 3)\
    _MAKE(  5, "取根节点", GetRootNode, "获取XML文档的根节点，返回XML节点句柄", \
    -1, _CMD_OS(OS_ALL), SDT_INT, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_exmlparser_global_var + 0)\
    _MAKE(  6, "取文档说明", GetDeclaration, "获取XML文档的说明信息，返回以逗号分隔的文本内容", \
    -1, _CMD_OS(OS_ALL), SDT_TEXT, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_exmlparser_global_var + 0)\
    _MAKE(  7, "取子节点数", GetChildCount, "获取指定XML节点的子节点数量，返回整数型", \
    -1, _CMD_OS(OS_ALL), SDT_INT, 0, LVL_SIMPLE, 0, 0, 1, g_argumentInfo_exmlparser_global_var + 4)\
    _MAKE(  8, "取属性", GetAttribute, "获取指定XML节点的属性，返回属性文本值", \
    -1, _CMD_OS(OS_ALL), SDT_TEXT, 0, LVL_SIMPLE, 0, 0, 2, g_argumentInfo_exmlparser_global_var + 5)\
    _MAKE(  9, "取属性数", GetAttributeCount, "获取指定XML节点的属性数量，返回整数型", \
    -1, _CMD_OS(OS_ALL), SDT_INT, 0, LVL_SIMPLE, 0, 0, 1, g_argumentInfo_exmlparser_global_var + 4)\
    _MAKE(  10, "取子节点", GetChildNode, "获取指定XML节点的子节点，返回XML节点句柄", \
    -1, _CMD_OS(OS_ALL), SDT_INT, 0, LVL_SIMPLE, 0, 0, 2, g_argumentInfo_exmlparser_global_var + 7)\
    _MAKE(  11, "添加节点", AddNode, "向指定节点添加子节点。该方法执行成功返回真，执行失败返回假", \
    -1, _CMD_OS(OS_ALL), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 2, g_argumentInfo_exmlparser_global_var + 9)\
    _MAKE(  12, "置属性", SetAttribute, "向指定节点添加属性。该方法执行成功返回真，执行失败返回假", \
    -1, _CMD_OS(OS_ALL), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 3, g_argumentInfo_exmlparser_global_var + 11)\
    _MAKE(  13, "插入节点", InstNode, "向节点指定位置插入子节点。该方法执行成功返回真，执行失败返回假", \
    -1, _CMD_OS(OS_ALL), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 3, g_argumentInfo_exmlparser_global_var + 14)\
    _MAKE(  14, "取所有属性", GetAttributes, "取指定节点的所有属性。返回属性的数量，属性数组的内容格式为：[名称，值，名称，值，...]", \
    -1, _CMD_OS(OS_ALL), SDT_INT, 0, LVL_SIMPLE, 0, 0, 2, g_argumentInfo_exmlparser_global_var + 17)\
    _MAKE(  15, "取所有节点", GetChildNodes, "取指定节点的所有子节点，返回子节点数量", \
    -1, _CMD_OS(OS_ALL), SDT_INT, 0, LVL_SIMPLE, 0, 0, 2, g_argumentInfo_exmlparser_global_var + 19)\
    _MAKE(  16, "移除节点", RemoveChild, "移除节点指定名称的第一个子节点。该方法执行成功返回真，执行失败返回假", \
    -1, _CMD_OS(OS_ALL), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 2, g_argumentInfo_exmlparser_global_var + 21)\
    _MAKE(  17, "移除属性", RemoveAttribute, "移除指定节点的属性。该方法执行成功返回真，执行失败返回假", \
    -1, _CMD_OS(OS_ALL), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 2, g_argumentInfo_exmlparser_global_var + 23)\
    _MAKE(  18, "释放文档", ReleaseXml, "释放XML文档占用的内存。该方法执行成功返回真，执行失败返回假", \
    -1, _CMD_OS(OS_ALL), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_exmlparser_global_var + 0)\
    _MAKE(  19, "创建节点", CreateNode, "创建XML节点，返回节点句柄", \
    -1, _CMD_OS(OS_ALL), SDT_INT, 0, LVL_SIMPLE, 0, 0, 1, g_argumentInfo_exmlparser_global_var + 25)\
    _MAKE(  20, "释放节点", ReleaseNode, "释放XML节点占用的内存。该方法执行成功返回真，执行失败返回假", \
    -1, _CMD_OS(OS_ALL), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 1, g_argumentInfo_exmlparser_global_var + 26)\
    _MAKE(  21, "添加注释", AddComment, "向指定节点添加注释。该方法执行成功返回真，执行失败返回假", \
    -1, _CMD_OS(OS_ALL), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 2, g_argumentInfo_exmlparser_global_var + 27)\
    _MAKE(  22, "取节点值", GetNodeValue, "获取指定节点的文本值", \
    -1, _CMD_OS(OS_ALL), SDT_TEXT, 0, LVL_SIMPLE, 0, 0, 1, g_argumentInfo_exmlparser_global_var + 4)\
    _MAKE(  23, "取节点名", GetNodeName, "获取指定节点的名称", \
    -1, _CMD_OS(OS_ALL), SDT_TEXT, 0, LVL_SIMPLE, 0, 0, 1, g_argumentInfo_exmlparser_global_var + 4)\
    _MAKE(  24, "置节点值", SetNodeValue, "设置节点的文本内容。该方法执行成功返回真，执行失败返回假", \
    -1, _CMD_OS(OS_ALL), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 2, g_argumentInfo_exmlparser_global_var + 30)\
    _MAKE(  25, "置节点名", SetNodeName, "设置节点的名称。该方法执行成功返回真，执行失败返回假", \
    -1, _CMD_OS(OS_ALL), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 2, g_argumentInfo_exmlparser_global_var + 32)\
    _MAKE(  26, "包含有属性", ContainsAttr, "指定节点存在属性返回真，否则返回假", \
    -1, _CMD_OS(OS_ALL), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 1, g_argumentInfo_exmlparser_global_var + 4)\
    _MAKE(  27, "包含有子级", ContainsChild, "指定节点存在子级节点返回真，否则返回假", \
    -1, _CMD_OS(OS_ALL), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 1, g_argumentInfo_exmlparser_global_var + 4)\
    _MAKE(  28, "存在属性", ExistsAttr, "指定节点存在指定名称的属性返回真，否则返回假", \
    -1, _CMD_OS(OS_ALL), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 2, g_argumentInfo_exmlparser_global_var + 34)\
    _MAKE(  29, "存在子节点", ExistsChild, "指定节点存在指定名称的子节点返回真，否则返回假", \
    -1, _CMD_OS(OS_ALL), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 2, g_argumentInfo_exmlparser_global_var + 36)